package FrontEnd;

import BackEnd.*;
import javafx.animation.PauseTransition;
import javafx.beans.Observable;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.AudioClip;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Screen;
import javafx.util.Duration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

/**
 * The controller for the LevelEditorScreen.fxml file.
 * This class provides functionality for various buttons allowing
 * the user to interact with the Level Editor, and bridges data
 * between the Level Editor and the actual Gameboard object.
 * @author CS-235 Group 43
 */
public class LevelEditorScreenController extends StateLoad {

    // Loading game audio
    private final String MAIN_MENU_SFX = "Assets\\SFX\\mainmenu.mp3";
    private final AudioClip MAIN_MENU_AUDIO =
            new AudioClip(new File(MAIN_MENU_SFX).toURI().toString());
    private final String RETURN_SFX = "Assets\\SFX\\return.mp3";
    private final AudioClip RETURN_AUDIO
            = new AudioClip(new File(RETURN_SFX).toURI().toString());
    private final String ERROR_SFX = "Assets\\SFX\\error.mp3";
    private final AudioClip ERROR_AUDIO
            = new AudioClip(new File(ERROR_SFX).toURI().toString());

    // Variables for the width and height of the board and tiles
    public static int optimalTileSize;
    private int width;
    private int height;

    // The gameboard to be edited
    private Gameboard g;

    // Self-explanatory
    private LevelEditorLogic levelEditorLogic;
    private boolean isSaved;
    private WindowLoader wl;
    private String boardName;
    private int numGoalTiles;
    private int maxGoalTiles;

    @FXML
    private Spinner<Integer> numGoalSpinner;

    @FXML
    private MenuBar menuBar;

    @FXML
    private VBox startingPosVBox;

    @FXML
    private VBox saveChangesPrompt;

    @FXML
    private Pane tiles;

    @FXML
    private Pane players;

    @FXML
    private Pane arrowPane;

    @FXML
    private Pane controls;

    @FXML
    private Pane selectedTile;

    @FXML
    private Pane fixed;

    @FXML
    private StackPane boardArea;

    @FXML
    private RadioButton fixedTileRadio;

    @FXML
    private RadioButton player1PosRadio;

    @FXML
    private RadioButton player2PosRadio;

    @FXML
    private RadioButton player3PosRadio;

    @FXML
    private RadioButton player4PosRadio;

    private RadioButton[] radioButtons;

    @FXML
    private ChoiceBox<String> tileSelection;

    @FXML
    private Spinner<Integer> t_tileSpinner;

    @FXML
    private Spinner<Integer> straight_tileSpinner;

    @FXML
    private Spinner<Integer> corner_tileSpinner;

    @FXML
    private Spinner<Integer> fire_tileSpinner;

    @FXML
    private Spinner<Integer> ice_tileSpinner;

    @FXML
    private Spinner<Integer> double_tileSpinner;

    @FXML
    private Spinner<Integer> back_tileSpinner;

    @FXML
    private TextField boardNameIn;

    @FXML
    private VBox setNamePrompt;

    @FXML
    private ScrollPane editorScrollBar;

    @FXML
    private VBox editorOptions;

    @FXML
    private Button changeTileButton;

    @FXML
    private Button rotateLeftButton;

    @FXML
    private Button rotateRightButton;

    private FloorTile tileSelected;

    @FXML
    private VBox errorBox;

    @FXML
    private Label errorMessage;

    @FXML
    private VBox topVBox;



    /**
     * Gets all resources for the Level Editor display
     * @param location  The location used to resolve relative paths for the root object, or
     *                  <tt>null</tt> if the location is not known.
     * @param resources The resources used to localize the root object, or <tt>null</tt> if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setIsLevelEditor(true);
        if (getInitData() != null) {
                //code for creating new default save (used in new custom game-board) when serializable classes are changed
            try {
                LevelEditorLogic.createDefaultSave();
            } catch (IOException e) {
                e.printStackTrace();
            }
            // instantiating game-board chosen from choice-box in levelEditorSetupController, "Default" if new game-board,
            try {
                g = FileReader.readCustomBoard(getInitData().get("Board"));
                boardName = getInitData().get("Board");
            } catch (FileNotFoundException e) {
                showError("FileNotFound: " + getInitData().get("Board") + ". basic 5x5 board created instead.", 3.5);
                g = new Gameboard(5, 5, null);
            }
            /*
            Disables all interactive elements when creating a new gameboard
            such that the user is forced to give the board a name.
             */
            if (getInitData().get("StartingState").equals("New")) {
                menuBar.setDisable(true);
                controls.setDisable(true);
                players.setDisable(true);
                fixed.setDisable(true);
                tiles.setDisable(true);
                editorOptions.setDisable(true);

                setNamePrompt.toFront();
                setNamePrompt.setVisible(true);

                numGoalTiles = 0;
                maxGoalTiles = 1;
                g.setNumGoalTiles(numGoalTiles);
                g.setMaxGoalTiles(maxGoalTiles);
            }

            /*
            Disables all interactive elements, to be re-enabled further down
             */
            else if (getInitData().get("StartingState").equals("Existing")) {
                fixedTileRadio.setDisable(true);
                tileSelection.setDisable(true);
                changeTileButton.setDisable(true);
                rotateRightButton.setDisable(true);
                rotateLeftButton.setDisable(true);
                startingPosVBox.setDisable(true);
                numGoalTiles = g.getNumGoalTiles();
                maxGoalTiles = g.getMaxGoalTiles();
                this.isSaved = true;
            }

            levelEditorLogic = new LevelEditorLogic(g);

            /*
            Stores the radio buttons in an array for easy access
             */
            radioButtons = new RadioButton[]{player1PosRadio, player2PosRadio,
                player3PosRadio, player4PosRadio};

            optimalTileSizeCalc();
            drawBoard();

            instantiateSpinners();

            /*
            Sets the action event for each spinner
             */
            t_tileSpinner.valueProperty().addListener(this::spinnerActionEvent);
            straight_tileSpinner.valueProperty().addListener(this::spinnerActionEvent);
            corner_tileSpinner.valueProperty().addListener(this::spinnerActionEvent);
            fire_tileSpinner.valueProperty().addListener(this::spinnerActionEvent);
            ice_tileSpinner.valueProperty().addListener(this::spinnerActionEvent);
            double_tileSpinner.valueProperty().addListener(this::spinnerActionEvent);
            back_tileSpinner.valueProperty().addListener(this::spinnerActionEvent);
            numGoalSpinner.valueProperty().addListener(this::goalActionEvent);
        }
    }

    /**
     * Calculates the optimal size for tiles to be displayed at based on the
     * width and height of the board.
     */
    private void optimalTileSizeCalc() {
        this.width = g.getWidth();
        this.height = g.getHeight();
        int availableWidth = (int) Screen.getPrimary().getBounds().getWidth()
            - Main.SIDE_BAR_WIDTH;
        int availableHeight = (int) Screen.getPrimary().getBounds().getHeight()
            - Main.MENU_BAR_HEIGHT;
        int maxTileWidth = availableWidth / ((g.getWidth() + 1) + Main.SIDE_SPACING);
        int maxTileHeight = availableHeight / ((g.getHeight() + 1) + Main.SIDE_SPACING);
        optimalTileSize = Math.min(maxTileHeight, maxTileWidth);
    }

    /**
     * Updates the values of the spinners based on
     * the number of tiles of each type in the Silk Bag.
     */
    public void updateSpinners() {
        t_tileSpinner.getValueFactory().setValue(g.getSilkbag().numberOfTilesOfType(TileType.T_SHAPE));
        straight_tileSpinner.getValueFactory().setValue(g.getSilkbag().numberOfTilesOfType(TileType.STRAIGHT));
        corner_tileSpinner.getValueFactory().setValue(g.getSilkbag().numberOfTilesOfType(TileType.CORNER));
        fire_tileSpinner.getValueFactory().setValue(g.getSilkbag().numberOfTilesOfType(TileType.FIRE));
        ice_tileSpinner.getValueFactory().setValue(g.getSilkbag().numberOfTilesOfType(TileType.FROZEN));
        double_tileSpinner.getValueFactory().setValue(g.getSilkbag().numberOfTilesOfType(TileType.DOUBLE_MOVE));
        back_tileSpinner.getValueFactory().setValue(g.getSilkbag().numberOfTilesOfType(TileType.BACKTRACK));

    }

    /**
     * Instantiates the spinners, assigning the initial value of a
     * spinner based on the number of tiles of that type in the silk bag.
     */
    public void instantiateSpinners() {
        // Assigns a value factory to each spinner
        t_tileSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory
                (0,1000, g.getSilkbag().numberOfTilesOfType(TileType.T_SHAPE)));
        straight_tileSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory
                (0,1000, g.getSilkbag().numberOfTilesOfType(TileType.STRAIGHT)));
        corner_tileSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory
                (0,1000, g.getSilkbag().numberOfTilesOfType(TileType.CORNER)));
        fire_tileSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory
                (0,1000, g.getSilkbag().numberOfTilesOfType(TileType.FIRE)));
        ice_tileSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory
                (0,1000, g.getSilkbag().numberOfTilesOfType(TileType.FROZEN)));
        double_tileSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory
                (0,1000, g.getSilkbag().numberOfTilesOfType(TileType.DOUBLE_MOVE)));
        back_tileSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory
                (0,1000, g.getSilkbag().numberOfTilesOfType(TileType.BACKTRACK)));
        numGoalSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory
                (1,(width * height)/2, maxGoalTiles));

        /*
         Forces the spinner's action event to run on focus loss such that
         values in the silk bag are correctly updated.
         */
        TextFormatter tFormatter = new TextFormatter(t_tileSpinner.getValueFactory().getConverter(), t_tileSpinner.getValueFactory().getValue());
        t_tileSpinner.getEditor().setTextFormatter(tFormatter);
        t_tileSpinner.getValueFactory().valueProperty().bindBidirectional(tFormatter.valueProperty());

        TextFormatter sFormatter = new TextFormatter(straight_tileSpinner.getValueFactory().getConverter(), straight_tileSpinner.getValueFactory().getValue());
        straight_tileSpinner.getEditor().setTextFormatter(sFormatter);
        straight_tileSpinner.getValueFactory().valueProperty().bindBidirectional(sFormatter.valueProperty());

        TextFormatter cFormatter = new TextFormatter(corner_tileSpinner.getValueFactory().getConverter(), corner_tileSpinner.getValueFactory().getValue());
        corner_tileSpinner.getEditor().setTextFormatter(cFormatter);
        corner_tileSpinner.getValueFactory().valueProperty().bindBidirectional(cFormatter.valueProperty());

        TextFormatter fFormatter = new TextFormatter(fire_tileSpinner.getValueFactory().getConverter(), fire_tileSpinner.getValueFactory().getValue());
        fire_tileSpinner.getEditor().setTextFormatter(fFormatter);
        fire_tileSpinner.getValueFactory().valueProperty().bindBidirectional(fFormatter.valueProperty());

        TextFormatter iFormatter = new TextFormatter(ice_tileSpinner.getValueFactory().getConverter(), ice_tileSpinner.getValueFactory().getValue());
        ice_tileSpinner.getEditor().setTextFormatter(iFormatter);
        ice_tileSpinner.getValueFactory().valueProperty().bindBidirectional(iFormatter.valueProperty());

        TextFormatter dFormatter = new TextFormatter(double_tileSpinner.getValueFactory().getConverter(), double_tileSpinner.getValueFactory().getValue());
        double_tileSpinner.getEditor().setTextFormatter(dFormatter);
        double_tileSpinner.getValueFactory().valueProperty().bindBidirectional(dFormatter.valueProperty());

        TextFormatter bFormatter = new TextFormatter(back_tileSpinner.getValueFactory().getConverter(), back_tileSpinner.getValueFactory().getValue());
        back_tileSpinner.getEditor().setTextFormatter(bFormatter);
        back_tileSpinner.getValueFactory().valueProperty().bindBidirectional(bFormatter.valueProperty());

        TextFormatter gFormatter = new TextFormatter(numGoalSpinner.getValueFactory().getConverter(), numGoalSpinner.getValueFactory().getValue());
        numGoalSpinner.getEditor().setTextFormatter(gFormatter);
        numGoalSpinner.getValueFactory().valueProperty().bindBidirectional(gFormatter.valueProperty());
    }

    /**
     * Draws the board on screen.
     */
    public void drawBoard() {

        // Clears the items from the tiles pane
        tiles.getChildren().clear();
        fixed.getChildren().clear();
        players.getChildren().clear();
        arrowPane.getChildren().clear();
        controls.getChildren().clear();

        // Sets the width and height for tiles
        tiles.setPrefWidth(width * optimalTileSize);
        tiles.setPrefHeight(height * optimalTileSize);
        fixed.setPrefWidth(width * optimalTileSize);
        fixed.setPrefHeight(height * optimalTileSize);

        //Sets the player pane and the tile button panes
        players.setPrefWidth(width * optimalTileSize);
        players.setPrefHeight(height * optimalTileSize);
        arrowPane.setPrefWidth(width * optimalTileSize);
        arrowPane.setPrefHeight(height * optimalTileSize);
        controls.setPrefWidth(width * optimalTileSize);
        controls.setPrefHeight(height * optimalTileSize);

        /*
        For the width, height of the board, add tiles to the tiles pane
        from the boardTiles arrayList in the Gameboard object
         */
        for (int x = 0; x < g.getWidth(); x++) {
            for (int y = 0; y < g.getHeight(); y++) {

                FloorTile tile = g.tileAt(new Coordinate(x, y));

                // Gets correct image
                Pane tileView = Assets.getFloorTileImage(tile, x, y, optimalTileSize);
                tiles.getChildren().add(tileView);

                // Adds a player image to represent the current players position
                for (int i = 0; i < FileReader.MAX_NUM_OF_PLAYERS; i++) {
                    if (g.getPlayerPos(i).equals(new Coordinate(x, y))) {
                        StackPane playerView = new StackPane();
                        ImageView playerCar = Assets.getPlayer(i);
                        String name = "Player " + (i + 1);
                        Text playerName = new Text(name);
                        playerName.setFill(Color.WHITE);
                        playerName.setFont(new Font("Arial", 15));
                        playerName.setStroke(Color.BLACK);
                        playerName.setStrokeWidth(0.5);
                        playerView.setTranslateX(x * optimalTileSize);
                        playerView.setTranslateY(y * optimalTileSize);
                        playerView.getChildren().addAll(playerCar, playerName);
                        players.getChildren().add(playerView);
                    }
                }

                // Adds an arrow indicating which tile is selected
                if (tileSelected != null) {
                    if (tileSelected.getLocation().equals(new Coordinate(x, y))) {
                        ImageView arrow = Assets.getLocationArrow();

                        arrow.setTranslateX(x * optimalTileSize);
                        arrow.setTranslateY(y * optimalTileSize);
                        arrowPane.getChildren().add(arrow);
                    }
                }

                // Sets the width and height for each tile
                Pane tileControl = new Pane();
                tileControl.setPrefWidth(optimalTileSize);
                tileControl.setPrefHeight(optimalTileSize);
                tileControl.setTranslateX(x * optimalTileSize);
                tileControl.setTranslateY(y * optimalTileSize);

                // Pseudo-final variables required for lambda expression
                int finalY = y;
                int finalX = x;


                // Adds functionality to the tile to be added to the pane
                tileControl.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {

                    //Enables tile editing options as there is a selected tile
                    tileSelection.setDisable(false);
                    changeTileButton.setDisable(false);
                    rotateRightButton.setDisable(false);
                    rotateLeftButton.setDisable(false);
                    startingPosVBox.setDisable(false);
                    editorOptions.setDisable(false);

                    // Outputs co-ordinate for debugging purposes
                    System.out.println(Integer.toString(finalX) + finalY);

                    //Updates the selected tile on the board
                    updateTileSelected(tile);

                    drawBoard();
                });
                controls.getChildren().add(tileControl);

            }
        }

    }

    /**
     * Updates the selected tile if a new tile position on the board is clicked.
     */
    private void updateTileSelected(FloorTile tile) {

        // Sets the display for the fixed tile radio and tile selection box
        tileSelected = tile;
        fixedTileRadio.setSelected(tileSelected.isFixed());

        /*
         Populates the choice box, excluding the type of the currently
         selected tile and goal tiles if there's already a goal tile
         */
        updateTileSelection();

        /*
        Disables the fixed tile radio button if the tile is a goal tile to
        ensure all goal tiles are fixed.
         */
        if (tileSelected != null) {
            fixedTileRadio.setDisable(tileSelected.getType() == TileType.GOAL);
        } else {
            fixedTileRadio.setDisable(false);
        }

        // Sets the display for the player position fixed tile radio
        for (int i = 0; i < FileReader.MAX_NUM_OF_PLAYERS; i++) {
            if (g.getPlayerPos(i).equals(tile.getLocation())) {
                radioButtons[i].setSelected(true);
                startingPosVBox.setDisable(true);
            } else {
                radioButtons[i].setSelected(false);
            }
        }


        // Updates the selected tile image
        selectedTile.getChildren().clear();
        selectedTile.getChildren().add(Assets.getFloorTileImage(
            tileSelected, 0, 0, (int) selectedTile.getWidth()));

    }

    /**
     * populates the tileSelection Combo-Box when a new tile is selected with allowed tile types.
     */
    private void updateTileSelection() {
        /*
         Clears tileSelection and sets the current
         value to the type of the selected tile
         */
        tileSelection.getItems().clear();
        tileSelection.setValue(tileSelected.getType().toString());

        // Populates the tile selection choice box
        // with valid TileTypes
        for (TileType tileType : new TileType[]{TileType.CORNER, TileType.STRAIGHT,
                TileType.T_SHAPE, TileType.GOAL}) {
            if (tileType != tileSelected.getType()) {
                if (tileType == TileType.GOAL && numGoalTiles < maxGoalTiles) {
                    tileSelection.getItems().add(tileType.toString());
                } else if (tileType != TileType.GOAL) {
                    tileSelection.getItems().add(tileType.toString());
                }
            }
        }
    }

    /**
     * Sets the selected tile to fixed
     */
    public void onSetFix() {
        levelEditorLogic.changeFixedTile(tileSelected.isFixed(), tileSelected);
        selectedTile.getChildren().clear();
        selectedTile.getChildren().add(Assets.getFloorTileImage(tileSelected, 0, 0, (int)
                selectedTile.getWidth()));
        this.isSaved = false;
        drawBoard();
    }

    /**
     * Changes the type of the selected tile to the
     * selected TileType.
     */
    public void applyTypeSelectionChange() {

        if (tileSelection.getValue() != null && !tileSelection.getValue().equals("BLANK")) {

            // If changing goal tile, decrement number of goal tiles
            if (tileSelected.getType() == TileType.GOAL) {
                numGoalTiles--;
                g.setNumGoalTiles(numGoalTiles);
            }

            // For goal tiles, disable the fixed tile radio
            if (TileType.valueOf(tileSelection.getValue()) == TileType.GOAL) {
                levelEditorLogic.changeFixedTile(false, tileSelected);
                fixedTileRadio.setSelected(true);
                fixedTileRadio.setDisable(true);
            } else {
                fixedTileRadio.setDisable(false);
            }

            // Changes the type of the selected tile
            tileSelected = levelEditorLogic.changeTileType(tileSelected, TileType.valueOf(tileSelection.getValue()));
            selectedTile.getChildren().clear();
            selectedTile.getChildren().add(Assets.getFloorTileImage(tileSelected, 0, 0, (int)
                    selectedTile.getWidth()));
            TileType t = tileSelected.getType();

            // If adding a goal tile, increment number of goal tiles
            if (t == TileType.GOAL) {
                numGoalTiles++;
                g.setNumGoalTiles(numGoalTiles);
            }

            updateSpinners();
            drawBoard();
            this.isSaved = false;
        }
    }

    /**
     * Rotates the selected tile anti-clockwise
     */
    public void onRotateLeftButton() {
        levelEditorLogic.rotateTileLeft(tileSelected);
        selectedTile.getChildren().clear();
        selectedTile.getChildren().add(Assets.getFloorTileImage(tileSelected, 0, 0, (int)
                selectedTile.getWidth()));
        drawBoard();
        this.isSaved = false;
    }

    /**
     * Rotates the selected tile clockwise
     */
    public void onRotateRightButton() {
        levelEditorLogic.rotateTileRight(tileSelected);
        selectedTile.getChildren().clear();
        selectedTile.getChildren().add(Assets.getFloorTileImage(tileSelected, 0, 0, (int)
                selectedTile.getWidth()));
        drawBoard();
        this.isSaved = false;
    }
    
    /**
     * Updates the starting position of a player based
     * on the selected radio button and tile
     */
    public void onSetStartPos(ActionEvent e) {
        startingPosVBox.setDisable(true);
        // gets the text attribute of the radio button that called the onAction from the event,
        // pass the int cast value to setStartingPos as  param,
        String posNum = ((RadioButton)e.getSource()).getText();
        levelEditorLogic.setStartingPos(Integer.parseInt(posNum) - 1, tileSelected);
        drawBoard();
        this.isSaved = false;
    }

    /**
     * Saves the game and sets isSaved to true.
     * @throws IOException Handles error identifiers
     * returned from FileReader.saveCustomBoard().
     */
    @FXML
    public boolean onSaveButton() throws IOException {
        boolean saveSuccessful = false;
        switch (FileReader.saveCustomBoard(g, boardName, maxGoalTiles)){
            case 0:
                showError("Save Successful", 1.5);
                this.isSaved = true;
                saveSuccessful = true;
                break;
            case 1:
                showError("Save failed: ensure no blank\ntiles are on the board!!", 3.5);
                ERROR_AUDIO.play(Double.parseDouble(getInitData().get("SFXVol")));
                break;
            case 2:
                showError("Save failed: Goal tile needs\nto be set as fixed!!", 3.5);
                ERROR_AUDIO.play(Double.parseDouble(getInitData().get("SFXVol")));
                break;
            case 3:
                showError("Save failed: please make sure\nyou have placed " + maxGoalTiles + " goal tiles!!", 3.5);
                ERROR_AUDIO.play(Double.parseDouble(getInitData().get("SFXVol")));
                break;
            case 4:
                showError("Save failed: please make sure\nyou have added at least \n1 Floor-tile to the silk bag!!", 3.5);
                break;
            default:
                showError("File reader save custom board error is not recognised!!", 3.5);
                throw new IllegalArgumentException("File reader save custom board error is not recognised!!");
        }

        return saveSuccessful;
    }

    /**
     * Displays an error message on screen
     * @param msg The error message to display
     * @param duration The duration to display the message for in seconds
     */
    public void showError(String msg, double duration) {
        saveChangesPrompt.setVisible(false);

        errorMessage.setText(msg);
        // displays error message for given time before making the error box invisible, hides if clicked,
        PauseTransition saveLabelHide = new PauseTransition(Duration.seconds(duration));
        saveLabelHide.setOnFinished(event -> errorBox.setVisible(false));
        errorBox.toFront();
        errorBox.setVisible(true);

        saveLabelHide.play();
    }

    /**
     * Begins the process of quitting the game,
     * prompting the user to save if isSaved is false.
     */
    public void onQuitButton() {
        if (!isSaved){
            saveChangesPrompt.toFront();
            saveChangesPrompt.setVisible(true);
        } else {
            quit();
        }
    }

    /**
     * Saves the game then quits
     * @throws IOException If onSaveButton throws an IO Exception
     */
    public void quitWithSave() throws IOException{
        if (onSaveButton()) {
            quit();
        } else {
            saveChangesPrompt.setVisible(false);
        }
    }

    /**
     * Prompts the user to choose a name for the board
     */
    public void chooseName(){
        /*
        Checks if the name is already used, and ensures
        the user has not chosen a name/entered only whitespace
         */
        if (fileExists(boardNameIn.getText())) {
            setNamePrompt.setVisible(false);
            showError("File name already in use!!", 2);
            setNamePrompt.setVisible(true);
            return;
        }else if (boardNameIn.getText().trim().length() == 0){
            setNamePrompt.setVisible(false);
            showError("Please enter a valid file name!!", 2);
            setNamePrompt.setVisible(true);
            return;
        }
        // Sets the board name if the input is valid
        this.boardName = boardNameIn.getText();
        setNamePrompt.setVisible(false);
        menuBar.setDisable(false);
        controls.setDisable(false);
        players.setDisable(false);
        fixed.setDisable(false);
        tiles.setDisable(false);
        editorOptions.setDisable(false);
        fixedTileRadio.setDisable(true);
        tileSelection.setDisable(true);
        changeTileButton.setDisable(true);
        rotateRightButton.setDisable(true);
        rotateLeftButton.setDisable(true);
        startingPosVBox.setDisable(true);
    }

    /**
     * Checks if a file with a given name exists.
     * @param name Name of the file
     * @return True if exists, false otherwise
     */
    public boolean fileExists(String name) {
        try {
            FileReader.readCustomBoard(name);
            return true;
        } catch (FileNotFoundException e) {
            return false;
        }
    }

    /**
     * Exits the game, returning the user to the setup screen.
     */
    public void quit(){
        setIsLevelEditor(false);
        wl = new WindowLoader(boardArea);
        wl.load("LevelEditorSetup", getInitData());
        RETURN_AUDIO.play(Double.parseDouble(getInitData().get("SFXVol")));
        MAIN_MENU_AUDIO.play(Double.parseDouble(getInitData().get("SFXVol")));
    }

    /**
     * Updates the silk bag based on the values of
     * the spinners called by the change listeners on each tile Spinner.
     * @param observable Runs code if observable value changes for any spinner,
     */
    public void spinnerActionEvent(Observable observable){
        // updates the number of each tile in the silk bag whenever a tile spinner value changes,
        this.g.getSilkbag().updateSilkBag(new int[]{t_tileSpinner.getValue(), straight_tileSpinner.getValue(),
                corner_tileSpinner.getValue(), fire_tileSpinner.getValue(), ice_tileSpinner.getValue(),
                double_tileSpinner.getValue(), back_tileSpinner.getValue()});
        this.isSaved = false;
    }

    /**
     * Updates the maximum amount of goal tiles possible on the board.
     * @param observable Runs code if observable value changes
     * on the numGoalSpinner.
     */
    private void goalActionEvent(Observable observable) {
        if (numGoalSpinner.getValue() < numGoalTiles) {
            numGoalSpinner.getValueFactory().setValue(numGoalTiles);
            // Change to printError msg
            showError("Cannot reduce max goal tiles below the\n number of goal tiles already on the board!!", 2);
        } else {
            maxGoalTiles = numGoalSpinner.getValue();
            g.setMaxGoalTiles(maxGoalTiles);
            updateTileSelection();
        }
        this.isSaved = false;
    }

    /**
     * Increases the height of the board, adding a row of blank tiles to the
     * bottom of the board.
     */
    public void incHeight() {
        g.resize("incHeight");
        optimalTileSizeCalc();
        this.isSaved = false;
        drawBoard();
    }
    /**
     * Decreases the height of the board, removing the row of tiles
     * at the bottom of the board.
     */
    public void decHeight() {
        // Caps the minimum height to 3
        if (height > 3) {
            int goalsToRemove = g.resize("decHeight");
            numGoalTiles -= goalsToRemove;
            g.setNumGoalTiles(numGoalTiles);
            optimalTileSizeCalc();

            /*
            Updates the selected tile if the previously selected
            tile was removed removed.
             */
            if (tileSelected != null) {
                if (tileSelected.getLocation().getY() == this.height) {
                    int x = tileSelected.getLocation().getX();
                    updateTileSelected(
                        g.tileAt(new Coordinate(x, this.height - 1)));
                }
            }

            this.isSaved = false;
            drawBoard();
        } else {
            showError("Cannot reduce board Height further!!", 2);
        }
    }
    /**
     * Increases the width of the board, adding a column of blank tiles
     * to the right of the board.
     */
    public void incWidth(){
        g.resize("incWidth");
        optimalTileSizeCalc();
        this.isSaved = false;
        drawBoard();
    }
    /**
     * Decreases the width of the board, removing the rightmost column of
     * tiles.
     */
    public void decWidth(){
        // Caps the minimum width to 3
        if (width > 3) {
            int goalsToRemove = g.resize("decWidth");
            numGoalTiles -= goalsToRemove;
            g.setNumGoalTiles(numGoalTiles);
            optimalTileSizeCalc();

            /*
            Updates the selected tile if the previously selected
            tile was removed removed.
             */
            if (tileSelected != null) {
                if (tileSelected.getLocation().getX() == this.width) {
                    int y = tileSelected.getLocation().getY();
                    updateTileSelected(g.tileAt(new Coordinate(
                        this.width - 1, y)));
                }
            }

            this.isSaved = false;
            drawBoard();
        } else {
            showError("Cannot reduce board width further!!", 2);
        }
    }

    /**
     * Replaces blank tiles with a randomly selected tile type of
     * random orientation.
     */
    public void autoFillBlanks() {
        for (FloorTile f : g.boardTiles) {
            if (f.getType() == TileType.BLANK) {
                f.setType(selectRandomType());
                f.setRotation(selectRandomRotation());
            }
        }
        if (tileSelected != null) {
            updateTileSelected(g.tileAt(tileSelected.getLocation()));
        }
        drawBoard();
    }

    /**
     * Selects a random rotation value.
     * @return The generated rotation value
     */
    private Rotation selectRandomRotation() {
        Random rand = new Random();
        int randRot = rand.nextInt(4);

        switch (randRot) {
            case 1:
                return Rotation.UP;
            case 2:
                return Rotation.LEFT;
            case 3:
                return Rotation.RIGHT;
            default:
                return Rotation.DOWN;
        }
    }

    /**
     * Selects a random TileType
     * @return The generated TileType value
     */
    private TileType selectRandomType() {
        Random rand = new Random();
        int randType = rand.nextInt(3);

        switch (randType) {
            case 0:
                return TileType.STRAIGHT;
            case 1:
                return TileType.CORNER;
            case 2:
                return TileType.T_SHAPE;
            default:
                return TileType.BLANK;
        }
    }

}
