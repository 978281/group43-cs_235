package FrontEnd;

import javafx.animation.PauseTransition;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.scene.media.AudioClip;
import javafx.util.Duration;
import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;

/**
 * Use to control the level editor screen scene.
 *
 * @author CS-235 Group 43
 */
public class LevelEditorSetupController extends StateLoad {

    private static final double ERROR_DURATION = 3.5;

    private final String MAIN_MENU_SFX = "Assets\\SFX\\mainmenu.mp3";
    private final AudioClip MAIN_MENU_AUDIO =
        new AudioClip(new File(MAIN_MENU_SFX).toURI().toString());
    private final String RETURN_SFX = "Assets\\SFX\\return.mp3";
    private final AudioClip RETURN_AUDIO
        = new AudioClip(new File(RETURN_SFX).toURI().toString());
    private final String ERROR_SFX = "Assets\\SFX\\error.mp3";
    private final AudioClip ERROR_AUDIO
        = new AudioClip(new File(ERROR_SFX).toURI().toString());

    @FXML
    private Button startButton;

    @FXML
    private Button playButton;

    @FXML
    private Button loadButton;

    @FXML
    private Button profileButton;

    @FXML
    private Button leaderboardButton;

    @FXML
    private Button backButton;

    @FXML
    private VBox editConfirmation;

    @FXML
    private VBox deleteConfirmation;

    @FXML
    private VBox errorMessageBox;

    @FXML
    private Label errorMessageLabel;

    @FXML
    private ComboBox<String> boardSelection;

    @FXML
    private VBox optionsVBox;

    private WindowLoader wl;

    /**
     * Called when the screen first appears in the window.
     * Sets certain values of the window created from fxml.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Load boards from CustomGameboards into the dropdown box
        optionsVBox.toFront();
        if (boardSelection.getValue() == null) {
            String[] gameBoards;
            File gameBoardLocation = new File("CustomGameboards");
            gameBoards = gameBoardLocation.list();
            if (gameBoards != null) {
                for (String gameBoard : gameBoards) {
                    if (!boardSelection.getItems().contains(gameBoard)) {
                        if (!gameBoard.equals("Default")) {
                            boardSelection.getItems().add(gameBoard);
                        }
                    }
                }
            }
        }
        // Removes occupied space taken up by deleteConfirmation VBox
        deleteConfirmation.setManaged(false);
    }

    /**
     * Called when new game is clicked.
     * Opens game setup for custom boards.
     */
    public void playGame() {
        if (new File("CustomGameboards").list().length != 1) {
            wl = new WindowLoader(playButton);
            wl.load("GameSetup", getInitData());
            MAIN_MENU_AUDIO.play(Double.parseDouble(getInitData().get("SFXVol")));
        } else {
            errorMessageLabel.setText("There are no custom gameboards\nto start a new game on");
            PauseTransition noBoardsLabelHide = new PauseTransition(Duration.seconds(ERROR_DURATION));
            noBoardsLabelHide.setOnFinished(event -> {
                errorMessageBox.setVisible(false);
                optionsVBox.setDisable(false);
                optionsVBox.toFront();
            });
            errorMessageBox.toFront();
            optionsVBox.setDisable(true);
            errorMessageBox.setVisible(true);
            noBoardsLabelHide.play();
        }
    }

    /**
     * Called when load game is clicked.
     * Opens load game screen for custom saves.
     */
    public void loadGame() {
        if (new File("SaveData/CustomSave").list().length != 0) {
            wl = new WindowLoader(loadButton);
            wl.load("LoadGame", getInitData());
            MAIN_MENU_AUDIO.play(Double.parseDouble(getInitData().get("SFXVol")));
        } else {
            errorMessageLabel.setText("There are no saves on any\ncustom gameboards to load");
            PauseTransition noBoardsLabelHide = new PauseTransition(Duration.seconds(ERROR_DURATION));
            noBoardsLabelHide.setOnFinished(event -> {
                errorMessageBox.setVisible(false);
                optionsVBox.setDisable(false);
                optionsVBox.toFront();
            });
            errorMessageBox.toFront();
            optionsVBox.setDisable(true);
            errorMessageBox.setVisible(true);
            noBoardsLabelHide.play();
        }
    }

    /**
     * Called when new custom board is clicked.
     * Opens level editor for a new custom board.
     */
    public void launchLevelEditorNew() {
        // Correctly gets selected board name & adds to HashMap
        getInitData().put("Board", "Default");
        getInitData().put("StartingState", "New");

        wl = new WindowLoader(startButton);
        wl.load("LevelEditorScreen", getInitData());
        MAIN_MENU_AUDIO.play(Double.parseDouble(getInitData().get("SFXVol")));
    }

    /**
     * Called when load custom board is clicked.
     * Opens level editor for a saved custom board if yes is clicked on the
     * prompt.
     */
    public void launchLevelEditorExisting() {
        // Correctly gets selected board name & adds to HashMap
        System.out.println(boardSelection.getValue());
        if ((boardSelection.getValue() == null)) {
            ERROR_AUDIO.play(Double.parseDouble(getInitData().get("SFXVol")));
            boardSelection.setStyle("-fx-background-color:#ff0000;");
            boardSelection.setPromptText("Please Select a Gameboard");
            return;
        }
        optionsVBox.setDisable(true);
        editConfirmation.setVisible(true);
        editConfirmation.toFront();
        editConfirmation.setManaged(true);
    }

    /**
     * Opens the level editor to edit a custom gameboard.
     */
    @FXML
    public void editYes() throws FileNotFoundException {
        String gameBoard = boardSelection.getValue();
        getInitData().put("Board", gameBoard);
        getInitData().put("StartingState", "Existing");
        deleteSaveFiles(gameBoard);
        wl = new WindowLoader(startButton);
        wl.load("LevelEditorScreen", getInitData());
        MAIN_MENU_AUDIO.play(Double.parseDouble(getInitData().get("SFXVol")));
    }

    /**
     * End editing board mode without going to level editor screen.
     */
    @FXML
    public void editNo() {
        editConfirmation.setVisible(false);
        editConfirmation.setManaged(false);
        optionsVBox.setDisable(false);
        optionsVBox.toFront();
    }


    /**
     * Deletes the selected board.
     */
    public void deleteYes() throws FileNotFoundException {
        String boardName = boardSelection.getValue();
        File boardToDelete = new File("CustomGameboards\\" + boardName);
        File leaderboardToDelete = new File("SaveData\\CustomLeaderboards\\"
            + boardName + ".lb");
        if (boardToDelete.delete()) {
            boardSelection.getItems().remove(boardName);
        }
        leaderboardToDelete.delete();
        deleteSaveFiles(boardName);
        deleteConfirmation.setVisible(false);

        deleteConfirmation.setManaged(false);
        optionsVBox.setDisable(false);
        optionsVBox.toFront();

    }

    /**
     * End deleting board mode without deleting board.
     */
    public void deleteNo() {
        deleteConfirmation.setVisible(false);
        deleteConfirmation.setManaged(false);
        optionsVBox.setDisable(false);
        optionsVBox.toFront();
    }

    /**
     * Deletes any related saves of the given board.
     *
     * @param boardName Name of the board to delete save files of
     */
    private void deleteSaveFiles(String boardName) throws FileNotFoundException {
        File[] saveFiles = new File("SaveData\\CustomSave\\").listFiles();
        for (File saveFile : saveFiles) {
            Scanner readFile = new Scanner(saveFile);
            String fileBoard = readFile.next();
            readFile.close();
            if (fileBoard.equals(boardName)) {
                System.out.println(fileBoard + "==" + boardName);
                saveFile.delete();
            }
        }
    }

    /**
     * Called when profiles button is clicked.
     * Opens profile window.
     */
    public void profile() {
        wl = new WindowLoader(profileButton);
        wl.load("Profiles", getInitData());
        MAIN_MENU_AUDIO.play(Double.parseDouble(getInitData().get("SFXVol")));
    }

    /**
     * Called when leaderboard button is clicked.
     * Opens leaderboard window.
     */
    public void leaderboard() {
        wl = new WindowLoader(leaderboardButton);
        wl.load("Leaderboard", getInitData());
        MAIN_MENU_AUDIO.play(Double.parseDouble(getInitData().get("SFXVol")));
    }

    /**
     * Double checks that user wants to delete the board.
     */
    public void deleteBoard() {
        if ((boardSelection.getValue() == null)) {
            ERROR_AUDIO.play(Double.parseDouble(getInitData().get("SFXVol")));
            boardSelection.setStyle("-fx-background-color:red;");
            boardSelection.setPromptText("Please Select a Gameboard");
            return;
        }
        optionsVBox.setDisable(true);
        deleteConfirmation.setVisible(true);
        deleteConfirmation.toFront();
        deleteConfirmation.setManaged(true);
    }

    /***
     * Returns to main menu.
     */
    public void onBackButton() {
        setIsCustom(false);
        WindowLoader wl = new WindowLoader(backButton);
        wl.load("MenuScreen", getInitData());
        RETURN_AUDIO.play(Double.parseDouble(getInitData().get("SFXVol")));
    }

}
