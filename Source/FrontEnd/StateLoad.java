package FrontEnd;

import javafx.fxml.Initializable;

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

/**
 * Interface to set up each window when switching between scenes.
 * only one method initData to give setup information to that screen.
 *
 * @author Christian Sanger
 */
public abstract class StateLoad implements Initializable {

	public static final int CUSTOM_BOARD_SUBSTRING_CUTOFF = 0;
	public static final int BASE_BOARD_SUBSTRING_CUTOFF = 4;

	private static boolean isCustom;
	private static boolean isLevelEditor;
	private HashMap<String, String> initData;

	/**
	 * Called to initialize a controller after its root element has been
	 * completely processed.
	 *
	 * @param location  The location used to resolve relative paths for the root object, or
	 *                  <tt>null</tt> if the location is not known.
	 * @param resources The resources used to localize the root object, or <tt>null</tt> if
	 */
	@Override
	public abstract void initialize(URL location, ResourceBundle resources);

	/**
	 * Return boolean based on if the gameboards are custom or not
	 * @return True if custom boards are used
	 */
	public static boolean getIsCustom() {
		return isCustom;
	}

	/**
	 * Sets if the gameboards are custom or not
	 * @param isCustom True if custom boards are to be used
	 */
	protected static void setIsCustom(boolean isCustom) {
		StateLoad.isCustom = isCustom;
	}

	/**
	 * Return boolean based on if on the level editor or not
	 * @return True if on the level editor or not
	 */
	public static boolean getIsLevelEditor() {
		return isLevelEditor;
	}

	/**
	 * Sets if on the level editor or not
	 * @param isLevelEditor True if on the level editor or not
	 */
	protected static void setIsLevelEditor(boolean isLevelEditor) {
		StateLoad.isLevelEditor = isLevelEditor;
	}

	/**
	 * Return the initialising data for this controller
	 * @return hashmap where key is a game state and the value is its value
	 */
	public HashMap<String, String> getInitData() {
		return initData;
	}

	/**
	 * Sets the initialising data for the controller
	 * @param initData hashmap where key is a game state and the value is its value
	 */
	public void setInitData(HashMap<String, String> initData) {
		this.initData = initData;
	}

}
