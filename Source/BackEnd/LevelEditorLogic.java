package BackEnd;

import java.io.*;

/**
 * Contains the logic for the custom gameboard in the level editor
 *
 * @author CS-235 Group 43
 */
public class LevelEditorLogic {

    private Gameboard board;

    /**
     * Constructs level editor logic object for the given custom gameboard
     * @param board Custom gameboard using the level editor logic
     */
    public LevelEditorLogic(Gameboard board) {
        this.board = board;
    }

    /**
     * Changes the floor type of the given tile
     *
     * @param originalTile Tile to change floor tile type
     * @param newTileType  Tile type to change the tile to
     * @return Passed tile with the new tile type
     */
    public FloorTile changeTileType(FloorTile originalTile, TileType newTileType) {
        board.placeFloorTile(originalTile.getLocation(), newTileType);
        FloorTile newTile = board.tileAt(originalTile.getLocation());
        if (originalTile.isFixed()) {
            newTile.setFixed();
        } else {
            newTile.unfixTile();
        }
        newTile.setRotation(originalTile.getRotation());
        return newTile;
    }

    /**
     * Change is whether the tile is fixed or not
     *
     * @param fixed whether the tile should be fixed or not
     * @param tile  the tile that should  be changed
     */
    public void changeFixedTile(boolean fixed, FloorTile tile) {
        if (!fixed) {
            tile.setFixed();
        } else {
            tile.unfixTile();
        }
    }

    /**
     * createDefaultSave is used when creating a new default gameboard.
     */
    public static void createDefaultSave() throws IOException {
        Gameboard g = new Gameboard(9, 9, new SilkBag());
        for (int x = 0; x < g.getWidth(); x++) {
            for (int y = 0; y < g.getHeight(); y++) {
                try {
                    TileType tileType = g.getSilkbag().getFloorTile().getType();
                    g.placeFloorTile(new Coordinate(x, y), tileType);
                } catch (Exception e) {
                    e.getStackTrace();
                }
            }
        }
        FileReader.saveCustomBoardDefault(g, "Default");
        System.out.println("Successful Creation of Default");

    }

    /**
     * Rotates the given tile right
     *
     * @param tile The tile to be rotated
     */
    public void rotateTileRight(FloorTile tile) {
        switch (tile.getRotation()) {
            case UP:
                tile.setRotation(Rotation.RIGHT);
                break;

            case RIGHT:
                tile.setRotation(Rotation.DOWN);
                break;

            case DOWN:
                tile.setRotation(Rotation.LEFT);
                break;

            case LEFT:
                tile.setRotation(Rotation.UP);
                break;
        }
    }

    /**
     * Rotates the given tile left
     *
     * @param tile The tile to be rotated
     */
    public void rotateTileLeft(FloorTile tile) {
        switch (tile.getRotation()) {
            case UP:
                tile.setRotation(Rotation.LEFT);
                break;

            case LEFT:
                tile.setRotation(Rotation.DOWN);
                break;

            case DOWN:
                tile.setRotation(Rotation.RIGHT);
                break;

            case RIGHT:
                tile.setRotation(Rotation.UP);
                break;
        }
    }

    /**
     * populates the silkbag with a specified number of a specific tile.
     * If there are more, then insert tiles into the bag up to newNumOfTile.
     * If there are less, then remove tiles from the bag down to newNumOfTile.
     *
     * @param type         the type of tile
     * @param newNumOfTile the number of tile that should be in the silkbag
     */
    public void setTileNum(TileType type, int newNumOfTile) {
        int numOfTile = 0;

        // count the number of tile of the particular type
        for (Tile tile : board.getSilkbag().getAllTiles()) {
            if (tile.getType() == type) {
                numOfTile++;
            }
        }

        if (numOfTile < newNumOfTile) {
            for (int i = numOfTile; i <= newNumOfTile; i++) {
                if (Tile.isFloorTile(type)) {
                    board.getSilkbag().insertTile(new FloorTile(type));
                } else {
                    board.getSilkbag().insertTile(new ActionTile(type));
                }
            }
        } else if (numOfTile > newNumOfTile) {
            for (Tile tile : board.getSilkbag().getAllTiles()) {
                if (numOfTile == newNumOfTile) {
                    break;
                } else if (tile.getType() == type) {
                    board.getSilkbag().removeTile(tile);
                    numOfTile--;
                }
            }
        }
    }

    /**
     * Updates the starting position of the selected player based on the selected tile
     *
     * @param player Player selected
     * @param tile   Selected tile
     */
    public void setStartingPos(int player, FloorTile tile) {
        board.setPlayerPosBase(player, tile.getLocation());
    }

    /**
     * Renders a new board with the width and the height passed
     *
     * @param width  the width of the board
     * @param height the height of the board
     */
    public void renderNewBoard(int width, int height) {
        Gameboard newBoard = new Gameboard(width, height, board.getSilkbag());
        board = newBoard;
    }


}
