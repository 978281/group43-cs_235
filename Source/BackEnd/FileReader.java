package BackEnd;

import javafx.util.Pair;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * This class is a file reader class which will take in a given level file format text file, verify it
 * and create the gameboard, along with the fixed tiles in their correct locations, the players in their correct
 * locations, and the amount and type of each floor/action tile that will populate the silk bag.
 *
 * @author Christian Sanger, Atif Ishaq, Joshua Oladitan and CS-235 Group 43
 * @version 1.0
 */

public class FileReader {

    /*
    These static variables hold information about the number of players and the number
    of different tile types.
     */
    public static final int MAX_NUM_OF_PLAYERS = 4;
    private static final int NUM_OF_TILE_TYPES = TileType.values().length;


    /**
     * This method takes in the given level format file,
     * and create a gameboard and players for that game.
     *
     * @param filename    The name of the level file format text file.
     * @param silkBagSeed The seed used for this game.
     * @return pair where first element is the gameboard and second is the players.
     * @throws Exception issue with gameboard file
     */
    public static Pair<Gameboard, Player[]> gameSetup(String filename, int silkBagSeed) throws Exception {
        File input = new File("Gameboards\\" + filename);
        if (!input.exists()) {
            throw new FileNotFoundException(filename);
        }
        Scanner in = new Scanner(input);
        Scanner currentLine;
        SilkBag silkBag = new SilkBag(silkBagSeed);

        //// board config
        currentLine = new Scanner(in.nextLine());
        int width = currentLine.nextInt();
        int height = currentLine.nextInt();
        Gameboard gameboard = new Gameboard(width, height, silkBag);

        //// Creating players
        Player[] players = new Player[MAX_NUM_OF_PLAYERS];
        Coordinate[] playerPos = new Coordinate[MAX_NUM_OF_PLAYERS];
        gameboard.setNumOfPlayers(MAX_NUM_OF_PLAYERS);
        for (int i = 0; i < MAX_NUM_OF_PLAYERS; i++) {
            String nextLine = in.nextLine();
            currentLine = new Scanner(nextLine);
            int x = currentLine.nextInt();
            int y = currentLine.nextInt();
            playerPos[i] = new Coordinate(x, y);
            gameboard.setPlayerPos(i, new Coordinate(x, y));
            players[i] = new Player(silkBag, gameboard);
        }

        //// Filling SilkBag
        int[] tileTypeCount = new int[NUM_OF_TILE_TYPES];
        // Reading how many of each tile
        for (int tileType = 0; tileType < NUM_OF_TILE_TYPES; tileType++) {
            currentLine = new Scanner(in.nextLine());
            tileTypeCount[tileType] = currentLine.nextInt();
        }

        // putting them in the bag
        // for each tile type
        for (int tileType = 0; tileType < NUM_OF_TILE_TYPES; tileType++) {
            int numberOfThisTile = tileTypeCount[tileType];
            // for each tile that need to be added to silkbag
            for (int i = 0; i < numberOfThisTile; i++) {
                Tile newTile = Tile.createTile(TileType.values()[tileType]);
                silkBag.insertTile(newTile);
            }
        }

        //// Fill with random tiles
        Random r = new Random(silkBagSeed);
        ArrayList<Coordinate> slideLocations = gameboard.getSlideLocations();

        while (gameboard.isBoardNotFull()) {
            Coordinate toSlide = null;
            if (slideLocations.size() == 0) {
                throw new Exception("No slide locations");
            }

            for (int i = 0; i < slideLocations.size(); i++) {
                FloorTile tile = silkBag.getFloorTile();
                tile.setRotation(Rotation.values()[r.nextInt(4)]);
                toSlide = slideLocations.get(r.nextInt(slideLocations.size()));
                if (toSlide == null) {
                    throw new Exception("Null Slide location");
                }
                gameboard.playFloorTile(toSlide, tile);
            }
        }
        //// Fixed tiles
        currentLine = new Scanner(in.nextLine());
        int numberOfFixedTiles = currentLine.nextInt();
        for (int i = 0; i < numberOfFixedTiles; i++) {
            currentLine = new Scanner(in.nextLine());
            TileType tileType = TileType.valueOf(currentLine.next().toUpperCase());
            int x = currentLine.nextInt();
            int y = currentLine.nextInt();
            Coordinate location = new Coordinate(x, y);
            int rotationInt = currentLine.nextInt();
            Rotation rotation = Rotation.values()[rotationInt];
            FloorTile tile = new FloorTile(tileType, rotation);
            gameboard.placeFixedTile(tile, location);
        }

        // Place players back in correct location as they would have been shifted when tiles have been placed
        for (int i = 0; i < MAX_NUM_OF_PLAYERS; i++) {
            for (int j = 0; j < 4; j++) {
                gameboard.setPlayerPos(i, playerPos[i]);
            }
        }

        return new Pair<>(gameboard, players);
    }

    /**
     * This method takes in the given level format file, and checks to see that the file exists.
     *
     * @param gameBoard The name of the level file format text file.
     * @return in The scanner that iterates through the file.
     * @throws Exception if cannot create gameSetup
     */
    public static Pair<Gameboard, Player[]> gameSetup(String gameBoard) throws Exception {
        return gameSetup(gameBoard, (new Random()).nextInt());
    }

    /**
     * Gets a custom gameboard.
     * @author CS-235 Group 43
     * @param filename the name and location of the custom board
     * @throws FileNotFoundException If there is no file of the given custom gameboard
     * @return Custom gameboard
     */
    public static Gameboard readCustomBoard(String filename) throws FileNotFoundException {
        Gameboard game = new Gameboard(5,5, new SilkBag());
        try {
            FileInputStream in = new FileInputStream("CustomGameboards/" + filename);
            ObjectInputStream objInput = new ObjectInputStream(in);
            game = (Gameboard) objInput.readObject();
            in.close();
        } catch (InvalidClassException e) {
            throw new IllegalArgumentException("Board was serialized on an outdated version of a serialized class");
        } catch (FileNotFoundException e) {
           throw new FileNotFoundException();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return game;
    }

    /**
     * Saves the custom board to a text file.
     * @author CS-235 Group 43
     * @param board the board that is being saved
     * @param filename the name and location to save the file
     * @throws IOException throws an exception if the streams are interrupted
     * @return an int based on the error: 0 no errors, 1 unexpected tile on the board ,2 more than 1 goal tile, 3 less than 1 goal tile,4 silk bag doesnt have 1 item
     */
    public static int saveCustomBoard(Gameboard board, String filename, int numberOfGoalTiles) throws IOException {
        SilkBag silkbag = board.getSilkbag();
        FileOutputStream outS = null;
        int goalCount = 0;

        for (Tile tile : board.getBoardTiles()) {
            if (!(Tile.isFloorTile(tile)) || (tile.getType() == TileType.BLANK)){
                return 1; // 1 = unexpected tile
            }
            else if (tile.getType() == TileType.GOAL) {
                FloorTile fTile = (FloorTile) tile;
                if (!fTile.isFixed()) {
                    return 2; // Goal tile is not fixed
                }
                goalCount ++;
            }
        }


        if (goalCount != numberOfGoalTiles) {
            return 3;
        }

        //removes unexpected tile from the silk bag
        for(Tile tile:silkbag.getAllTiles()) {
            if (tile.getType() == TileType.BLANK || tile.getType() == TileType.GOAL) {
                board.getSilkbag().removeTile(tile);
            }
        }

        if(board.getSilkbag().numberOfFloorTiles() == 0) {
            return 4;
        }

        try {
            outS = new FileOutputStream("CustomGameboards/" + filename);
        }
        catch (FileNotFoundException e) {
            File newFile = new File("CustomGameboards/" + filename);
            try {
                newFile.createNewFile();
            }
            catch (IOException d) {
                throw new IOException();
            }
        }

        try {
            ObjectOutputStream outObj = new ObjectOutputStream(outS);
            outObj.writeObject(board);
        }
        catch (IOException e) {
            throw new IOException();
        }
        outS.close();
        return 0; // 0 = success
    }

    /**
     * Saves recreates the default save after serialized classes changed.
     * @author CS-235 Group 43
     * @param board the board that is being saved
     * @param filename the name and location to save the file
     * @throws IOException throws an exception if the streams are interrupted
     */
    public static void saveCustomBoardDefault(Gameboard board, String filename) throws IOException {
        FileOutputStream outS = null;

        try {
            outS = new FileOutputStream("CustomGameboards/" + filename);
        }
        catch (FileNotFoundException e) {
            File newFile = new File("CustomGameboards/" + filename);
            try {
                newFile.createNewFile();
            }
            catch (IOException d) {
                throw new IOException();
            }
        }

        try {
            ObjectOutputStream outObj = new ObjectOutputStream(outS);
            outObj.writeObject(board);
        }
        catch (IOException e) {
            throw new IOException();
        }
    }
}