package BackEnd;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Random;

/**
 * This class represents the Silk bag that holds and distributes tiles for the game.
 *
 * @author James Sam
 * @version 1.0
 */

public class SilkBag implements Serializable {

    /*
     * These are variables are used for the silk bag, a list to hold all tiles and
     * a random generator to generate a random tile.
     */
    private final ArrayList<Tile> allTiles;
    private final Random randomGenerator;

    /*
     * This is a constant to save the seed for loading the game.
     */
    private final int SEED;

    /**
     * First constructor of the silk bag, which initialises attributes.
     * Giving a random integer seed for the random generator.
     *
     * @param seed the integer seed of the random generator.
     */
    public SilkBag(int seed) {
        allTiles = new ArrayList<>();
        randomGenerator = new Random(seed);
        this.SEED = seed;
    }

    /**
     * custom blank constructor for default save.
     * @author CS-235 Group 43
     */
    public SilkBag() {
        allTiles = new ArrayList<>();
        for (int i = 0; i < (9*9); i++) {
            insertTile(new FloorTile(TileType.BLANK));
        }
        this.SEED = 0;
        randomGenerator = new Random(SEED);
    }
    
    /**
     * returns all tiles inside the silk bag
     * @return  returns all tiles inside the silk bag
     */
    public ArrayList<Tile> getAllTiles() {
        return allTiles;
    }


    /**
     * This method gets a random tile from the bag, also removes it from the bag
     *
     * @return Tile, A random tile that was generated.
     */
    public Tile getTile() {
        int index = randomGenerator.nextInt(allTiles.size());
        return allTiles.remove(index);
    }

    /**
     * This method gets a floor tile, for the game setup of the board.
     *
     * @return tile, A random floor tile.
     * @throws Exception
     */
    public FloorTile getFloorTile() throws Exception {
        // Create a random index.
        int index = randomGenerator.nextInt(allTiles.size());
        // Note the index we started at.
        int startIndex = index;
        Tile tile = allTiles.get(index);
        // Loop though from starting index until we have a floor tile
        while (!Tile.isFloorTile(tile)) {
            index = (index + 1) % allTiles.size();
            tile = allTiles.get(index);
            if (index == startIndex) {
                // We have looped and found no floor tile
                throw new Exception("No FloorTile in Silk bag");
            }
        }
        if (!allTiles.remove(tile)) {
            System.out.println("dfg");
        }
        return (FloorTile) tile;
    }

    /**
     * This method inserts a tile into the bag.
     *
     * @param tile, tile to be inserted into the bag.
     */
    public void insertTile(Tile tile) {
        allTiles.add(tile);
    }

    /**
     * Removes a specified tile from the silk bag
     *
     * @param tile The tile to remove
     */
    public void removeTile(Tile tile) {
        allTiles.remove(tile);
    }

    /**
     * Checks how many tiles of a given type there are in the silk bag.
     * @param type The tile type to search for
     * @return The number of tiles of that type
     * @author CS-235 Group 43
     *
     */
    public int numberOfTilesOfType(TileType type){
        int count = 0;
        for (Tile tile: allTiles) {
            if (tile.getType() == type){
                count++;
            }
        }
        return count;
    }


    /**
     * Clears the silk bag and adds in tiles from the input array
     * @param tilesNum An array containing ints representing each tile type.
     * @author CS-235 Group 43
     */
    public void updateSilkBag(int[] tilesNum){
        this.allTiles.removeAll(allTiles);
        int i=0;
        switch (i){
            case  (0):
                for (int x = 0; x < tilesNum[i]; x++){
                    insertTile(new FloorTile(TileType.T_SHAPE));
                }
                i++;
            case  (1):
                for (int x = 0; x < tilesNum[i]; x++){
                    insertTile(new FloorTile(TileType.STRAIGHT));
                }
                i++;
            case  (2):
                for (int x = 0; x < tilesNum[i]; x++){
                    insertTile(new FloorTile(TileType.CORNER));
                }i++;
            case  (3):
                for (int x = 0; x < tilesNum[i]; x++){
                    insertTile(new ActionTile(TileType.FIRE));
                }
                i++;
            case  (4):
                for (int x = 0; x < tilesNum[i]; x++){
                    insertTile(new ActionTile(TileType.FROZEN));
                }
                i++;
            case  (5):
                for (int x = 0; x < tilesNum[i]; x++){
                    insertTile(new ActionTile(TileType.DOUBLE_MOVE));
                }
                i++;
            case  (6):
                for (int x = 0; x < tilesNum[i]; x++){
                    insertTile(new ActionTile(TileType.BACKTRACK));
                }
                i++;
            default:
                System.out.println("finished");
                break;
        }

    }

    /**
     * Returns the number of tiles in the silk bag
     * @return an integer value of the amount of tiles in the bag
     * @author CS-235 Group 43
     */
    public int numOfTiles(){
        return allTiles.size();
    }
    public int numberOfFloorTiles(){
        int num = 0;
        for (Tile t : allTiles){
            if (t instanceof FloorTile){
                num++;
            }
        }
        return num;
    }
}